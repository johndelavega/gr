//
// gr version 002
//
// 05:28 PM 03/03/2018, Saturday
//
// go routines / goroutines

// https://gobyexample.com/closing-channels
// https://play.golang.org/p/eFZ2SeKswH

// _Closing_ a channel indicates that no more values
// will be sent on it. This can be useful to communicate
// completion to the channel's receivers.

package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"
)

// In this example we'll use a `jobs` channel to
// communicate work to be done from the `main()` goroutine
// to a worker goroutine. When we have no more jobs for
// the worker we'll `close` the `jobs` channel.
func main() {
	//	jobs := make(chan int, 5)
	//	done := make(chan bool)
	//	stop1 := make(chan bool, 5)
	moreChan := make(chan bool)

	// Here's the worker goroutine. It repeatedly receives
	// from `jobs` with `j, more := <-jobs`. In this
	// special 2-value form of receive, the `more` value
	// will be `false` if `jobs` has been `close`d and all
	// values in the channel have already been received.
	// We use this to notify on `done` when we've worked
	// all our jobs.
	go func() {
		for {
			fmt.Println(" go for")
			// j, more := <-jobs // this prevents the for loop from continues running
			j := 1
			more := <-moreChan // this prevents the for loop from continues running
			fmt.Print(" for  ", j, " ")
			if more {
				fmt.Printf("\r%s", strings.Repeat(" ", 15))
				//				fmt.Printf("sleep 50 ms, rand100 = %d", rand.Intn(100))
				fmt.Print("more is true, received job  ", j)
				//				fmt.Println("received job", j)
			} else if j == -1 {
				fmt.Println("received all jobs, bool more =", more)
				//	done <- true
				return
			}
		}
	}()

	// // This sends 3 jobs to the worker over the `jobs`
	// // channel, then closes it.
	// for j := 1; j <= 3; j++ {
	// 	jobs <- 2
	// 	fmt.Println("sent job", j)
	// }
	// close(jobs)
	// fmt.Println("sent all jobs")

	// for {

	// 	jobs <- rand.Intn(100)
	// 	time.Sleep(time.Second)

	// }

	//	fmt.Println(" stop1 = ", stop1)

	var b1 = false

	go func() {
		for {

			if b1 {
				//	jobs <- 0
				return
			}

			//	jobs <- rand.Intn(10000)
			time.Sleep(time.Second)
			//fmt.Println("send for")

			//			a, b := <-stop1
			//fmt.Println(a,b)

			// if false {
			// 	b := <-stop1
			// 	fmt.Println(b)
			// }

			// if b {
			// 	fmt.Println("\n stopped sending jobs")
			// 	return
			// }

		}
	}()

	//	rand.Intn(100)

	// We await the worker using the
	// [synchronization](channel-synchronization) approach
	// we saw earlier.
	//	<-done

mainloop:

	for {

		// fmt.Println("scan line")
		// fmt.Scanln()

		fmt.Print("\n - bufio reader, x to exit -- ")

		reader := bufio.NewReader(os.Stdin)
		key, _ := reader.ReadString('\n')

		//	jobs <- rand.Intn(100)

		if strings.Contains(key, "x") {
			fmt.Printf("\n\nkey pressed = %s\r", key)
			b1 = true
			//moreChan <- true
			//close(jobs)
			//<-done
			//<-moreChan
			//return
			break mainloop
		}

		if strings.Contains(key, "t") {
			moreChan <- true
		}
		if strings.Contains(key, "f") {
			moreChan <- false
		}

		if strings.Contains(key, "s") {
			//			fmt.Printf("key pressed = %s\r", key)
			if !b1 {
				//fmt.Printf("  -- close(jobs) | key pressed = %s\r", key)
				fmt.Printf("  --  key pressed = %s\r", key)
				//	close(jobs)
				//	<-done
			}
			b1 = true

		}

	}

	//	<-moreChan

	// var count int64 = 0

	// for {

	// 	fmt.Printf("\r%s", strings.Repeat(" ", 15))
	// 	fmt.Printf("sleep 50 ms, count = %d", count)

	// 	// windows10/task manager/details/cpu = 0 at 50 milliseconds
	// 	// cpu = 0 if sleep 50 ms
	// 	// cpu = 10 if sleep commented out and printf count
	// 	// cpu = 25 if sleep commented out and printf count also commented out
	// 	//		time.Sleep(time.Millisecond * 50)

	// 	time.Sleep(time.Second)

	// 	count++

	// }

}
